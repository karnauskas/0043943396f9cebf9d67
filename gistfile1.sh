[Unit]
Description=CloudBeees On Prem executor
Documentation=https://developer.cloudbees.com/bin/view/DEV/On-Premise+Executors
Requires=network.target
After=multi-user.target

[Service]
Type=simple
ExecStart=/opt/java6/bin/java -jar /usr/share/jenkins-cli.jar -s https://cloudbees.ci.cloudbees.com on-premise-executor -fsroot /workspace -labels docker -nam\
e docker-builder-1
Restart=always
RestartSec=60
StartLimitInterval=0
User=<changeme>

[Install]
WantedBy=multi-user.target

#
# To install: 
#
# ensure all names and paths above are correct for you: 
#
# install -D -m 644 ope.service /usr/lib/systemd/system/ope.service
# systemctl daemon-reload
# systemctl start ope
# systemctl enable op